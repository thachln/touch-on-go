/*
Copyright 2019 The D2M Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"io/ioutil"
	"log"
	"os"
	s "strings"

	"datacore"

	"gopkg.in/yaml.v2"
)

var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
)

func init() {
	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	InfoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

type conf struct {
	New struct {
		FuncName string `yml:"func_name"`
	} `yml:"new"`
	Edit struct {
		FuncName string `yml:"func_name"`
	} `yml:"edit"`
	Del struct {
		FuncName string `yml:"func_name"`
	} `yml:"del"`
	Exp struct {
		FuncName string `yml:"func_name"`
	} `yml:"exp"`
}

func (c *conf) getConfig() *conf {

	yamlFile, err := ioutil.ReadFile("datacmd.yml")
	if err != nil {
		ErrorLogger.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		ErrorLogger.Fatalf("Unmarshal: %v", err)
	}

	return c
}

func main() {
	InfoLogger.Println("Starting the application...")
	// validate
	if len(os.Args) < 2 {
		ErrorLogger.Println("Missing parameter")
		return
	}

	// get table from file name
	inputParam := os.Args[1]
	splitInput := s.Split(inputParam, "\\")
	nameFile := splitInput[len(splitInput)-1]
	splitFileName := s.Split(nameFile, "_")
	tableName := splitFileName[0]

	// get action from file name
	actionSplit := s.Split(splitFileName[1], ".")
	action := actionSplit[0]

	// get config
	var c conf
	c.getConfig()

	switch action {
	case "new":
		datacore.Process_new(tableName, os.Args)
	case "edit":
		datacore.Process_edit(tableName, os.Args[1])
	case "del":
		datacore.Process_del(tableName, os.Args[1])
	case "exp":
		datacore.Process_exp(tableName, os.Args[1])
	}
}
