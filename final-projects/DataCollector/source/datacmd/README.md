# Project Name

datacmd

### Dependencies
* go
* mysql
* create database before run
* init module

## Executing program
build file datacmd.exe
```
go build -o datacmd.exe
```

# example
insert data:
```
datacmd E:\Golang\product_new.csv INT VARCHAR*255 VARCHAR*255 TIMESTAMP INT
```

edit data:
```
datacmd E:\Golang\product_edit.csv
```

delete data:
```
datacmd E:\Golang\product_del.csv
```

export data:
```
datacmd E:\Golang\product_exp.csv
```