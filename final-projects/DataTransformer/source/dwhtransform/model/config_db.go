package model

type ConfigDB struct {
	Database struct{
		Type string `yaml:"type"`
		Host string `yaml:"host"`
		Port string `yaml:"port"`
		Name string `yaml:"name"`
		User string `yaml:"user"`
		Password string `yaml:"password"`
		SSLMode string `yaml:"sslmode"`
	}
}