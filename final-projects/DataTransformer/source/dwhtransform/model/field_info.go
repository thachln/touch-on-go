package model

type FieldInfo struct {
	ColumnName    string
	TypeName      string
	IsNullable    string
	DecimalDigits string
	ColumnSize    string
	PkKey         string
}