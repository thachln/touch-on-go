package utils

import "strings"

func getIndexStr(str string, char string) int {
	idx := strings.Index(str, char)
	return idx
}