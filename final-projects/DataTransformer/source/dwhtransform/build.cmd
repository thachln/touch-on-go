@REM cd program
@REM cd genschema
@REM go build -o genschema.exe
@REM cd ../
@REM cd importschema
@REM go build -o importschema.exe
@REM cd ../
@REM cd transfer
@REM go build -o transfer.exe
@REM cd ../../
@REM go build -o dwhtf.exe

@REM install submodule
go install ./program/genschema ./program/importschema ./program/transfer

@REM build file dwhtf.exe
go build -o dwhtf.exe