package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"gitlab.com/ducpt51/dwhtf/config"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/model"
	"gitlab.com/ducpt51/dwhtf/repository"
	"gitlab.com/ducpt51/dwhtf/utils"
	"log"
)

type ConnectDB struct {
	LogDW *logdwhtf.Logger
}

func NewConnectDB(logDW *logdwhtf.Logger) repository.HandleConnectI {
	return ConnectDB{logDW}
}

func (c ConnectDB) ConnectDB(fileName string) (db *sql.DB, conf *model.ConfigDB) {
	cf := config.NewConfigRepository(c.LogDW)
	dbConfig, err := cf.GetConfig(fileName)
	dataSourceName := setDataSourceName(dbConfig)
	if err != nil {
		log.Fatal(err)
	}
	c.LogDW.LogServer(utils.DebugLog).Printf("Data Config: %v\n", dbConfig)
	db, err = sql.Open(dbConfig.Database.Type, dataSourceName)
	if err != nil {
		c.LogDW.LogServer(utils.ErrorLog).Println("Open connection failed:", err.Error())
		panic(err.Error())
	}
	conf = &dbConfig
	return db, conf
}
func setDataSourceName( dbConfig model.ConfigDB) string {
	var dataSourceName string
	switch dbConfig.Database.Type {
	case utils.MySQl:
		dataSourceName = dbConfig.Database.User+":"+dbConfig.Database.Password+"@/"+dbConfig.Database.Name
	case utils.Postgres:
		dataSourceName = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
			dbConfig.Database.Host, dbConfig.Database.Port, dbConfig.Database.User,
			dbConfig.Database.Password, dbConfig.Database.Name, dbConfig.Database.SSLMode)
	}
	return dataSourceName
}