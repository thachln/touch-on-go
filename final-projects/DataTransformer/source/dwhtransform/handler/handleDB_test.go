package handler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ducpt51/dwhtf/database"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/repository"
	"io/ioutil"
	"testing"
)

var logF *logdwhtf.Logger
var conn database.ConnectDB

func initNewProcessDB() repository.HandleDBI {
	return NewProcessDB(logF, conn)
}

func TestDBRepository_ExportStruct(t *testing.T) {
	i := initNewProcessDB()
	outF := "../output_file"
	path := "../config/source.yml"
	err := i.ExportStruct(path, outF)
	files, _ := ioutil.ReadDir(outF + "/")
	assert.Nil(t, err)
	assert.Equal(t, len(files) > 0, true)

	// abNormal case
	outF = "output_file"
	path = "../config/source.yml"
	err = i.ExportStruct(path, outF)
	assert.NotNil(t, err)
	assert.Contains(t,  err.Error(), "The system cannot find the path specified")
}

func TestDBRepository_ImportStruct(t *testing.T) {
	i := initNewProcessDB()
	inF := "../input_file"
	path := "../config/dest.yml"
	err := i.ImportStruct(path, inF)
	assert.Nil(t, err)

	//AbNormal case
	inF = "input_file"
	path = "../config/dest.yml"
	err = i.ImportStruct(path, inF)
	assert.NotNil(t, err)
	assert.Contains(t,  err.Error(), "The system cannot find the file specified")
}

func TestDBRepository_TransferData(t *testing.T) {
	i := initNewProcessDB()
	sFile := "../config/source.yml"
	dFile := "../config/dest.yml"
	mapFile := "../mapping.csv"
	err := i.TransferData(sFile, dFile, mapFile)
	assert.Nil(t, err)

	//AbNormal case
	mapFile = "map.csv"
	err = i.TransferData(sFile, dFile, mapFile)
	assert.NotNil(t, err)
	assert.Contains(t,  err.Error(), "The system cannot find the file specified")
}