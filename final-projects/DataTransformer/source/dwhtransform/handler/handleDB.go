package handler

import (
	"context"
	"database/sql"
	"encoding/csv"
	"errors"
	"fmt"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/repository"
	"gitlab.com/ducpt51/dwhtf/utils"
	"io/ioutil"
	"os"
	strs "strings"
)


// DBRepository ...
type DBRepository struct {
	LogDW *logdwhtf.Logger
	Conn repository.HandleConnectI
}

func NewProcessDB( logDW *logdwhtf.Logger, conn repository.HandleConnectI) repository.HandleDBI {
	return &DBRepository{ logDW, conn}
}

func (d DBRepository) ExportStruct(pathFile string, outFolder string) error {
	// connect db
	db, _ := d.Conn.ConnectDB(pathFile)
	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			return
		}
	}(db)

	nameTables, err := db.Query("SHOW TABLES")
	if err != nil {
		d.LogDW.LogServer(utils.ErrorLog).Println("Get list table name fail: ", err.Error())
		return err
	}
	var tables []string
	var name string
	for nameTables.Next() {
		err = nameTables.Scan(&name)
		if err != nil {
			return err
		}
		tables = append(tables, name)
	}
	d.LogDW.LogServer(utils.DebugLog).Println("List table: ", tables)
	for _, t := range tables {
		sqlStruct := `DESCRIBE ` + t
		infoTable, errT := db.Query(sqlStruct)
		if errT != nil {
			d.LogDW.LogServer(utils.ErrorLog).Printf("Get struct table %s fail", t)
			return err
		}
		infoFields, err := infoTable.Columns()
		if err != nil {
			d.LogDW.LogServer(utils.ErrorLog).Printf("Not get infoField failed %s", err.Error())
			return err
		}
		infoFs := make([]sql.RawBytes, len(infoFields))
		scanInfoF := make([]interface{}, len(infoFs))
		for i := range infoFs {
			scanInfoF[i] = &infoFs[i]
		}
		//Get contents of all lines
		data := make([][]string, 0)
		for infoTable.Next() {
			//Save the contents of each line
			var infoL []string
			//Add the contents of each line to scanInfoF, and also to infoFs
			err = infoTable.Scan(scanInfoF...)
			if err != nil {
				d.LogDW.LogServer(utils.ErrorLog).Printf(" Scan data fail: %v", err)
				return err
			}
			for _, v := range infoFs {
				infoL = append(infoL, string(v))
			}
			// convert data according to format file
			d := convertData(infoL)
			data = append(data, d)
		}
		if err = infoTable.Err(); err != nil {
			return err
		}
		d.LogDW.LogServer(utils.DebugLog).Printf("data export of table %s: %v", t, data)
		file := outFolder + "/" + t + ".csv"
		columns := []string{"COLUMN_NAME", "TYPE_NAME", "IS_NULLABLE", "DECIMAL_DIGITS", "COLUMN_SIZE", "PK_KEY"}
		err = writeToCSV(file, columns, data, d.LogDW)
		if err != nil {
			d.LogDW.LogServer(utils.ErrorLog).Printf("Export info table %s fail: %v", t, err)
			return err
		}
		fmt.Printf("Export table %s successful! \n", t)
	}
	return nil
}

func (d DBRepository) ImportStruct(pathFile string, inputFolder string) error {
	// read data
	db, conf := d.Conn.ConnectDB(pathFile)
	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			return
		}
	}(db)

	files, err := ioutil.ReadDir(inputFolder + "/")
	if err != nil {
		return err
	}
	for _, file := range files {
		f := file.Name()
		tableName := f[: strs.Index(f, ".")]
		infoTables, err := readFile(inputFolder + "/" + f, d.LogDW)
		if err != nil {
			return err
		}
		//create table
		pKey := ""
		sqlCreateTable := `CREATE TABLE IF NOT EXISTS ` + tableName + `(`
		for key, col := range infoTables {
			if key < 1 {
				continue
			}
			colName := col[0]
			typeCol := col[1]
			idx := strs.Index(typeCol, " ")
			var size string
			if col[3] != "" {
				size = col[3]
			}
			if col[4] != "" {
				size = col[4]
			}
			if idx != -1 {
				typeCol = col[1][:idx] + "(" + size + ")" + col[1][idx:]
			}
			if col[2] == "NO" {
				typeCol += " NOT NULL "
			} else if col[2] == "YES" {
				typeCol += " NULL "
			}
			if col[5] == "YES" {
				//typeCol += " PRIMARY KEY "
				pKey += colName + `, `
			}
			if conf.Database.Type == utils.Postgres {
				ckType := strs.Contains(typeCol, "unsigned")
				if ckType {
					typeCol = " SERIAL "
				}
				//else if ckType && col[5] == "NO" {
				//	typeCol = " SERIAL "
				//}
			}
			sqlCreateTable += colName + ` ` + typeCol + `,`
		}
		pKey = strs.TrimSuffix(pKey, `, `)
		sqlCreateTable += `PRIMARY KEY (` + pKey + `)`
		sqlCreateTable = strs.TrimSuffix(sqlCreateTable, `,`)
		sqlCreateTable += `);`
		// log
		d.LogDW.LogServer(utils.DebugLog).Println(" " + sqlCreateTable)
		_, err = db.Exec(sqlCreateTable)
		if err != nil {
			d.LogDW.LogServer(utils.ErrorLog).Printf("Create table %s fail because %v \n", tableName, err)
			return err
		}
		fmt.Printf("Import table %s successful! \n", tableName)
	}
	return nil
}

func (d DBRepository) TransferData(sPath string, dPath string, fMap string) error {
	// Get data table source
	// get data from file mapping
	dataList, err := readFile(fMap, d.LogDW)
	if err != nil {
		return err
	}
	sTable := dataList[1][0]
	dTable := dataList[1][2]
	var sCols [] string
	var dCols [] string
	funcList := make(map[int]string)
	for i, row := range dataList {
		if i < 1 {
			continue
		}
		sCols = append(sCols, row[1])
		dCols = append(dCols, row[3])
		funcList[i] = row[4]
	}
	sLen := len(sCols)
	dLen := len(dCols)
	if sLen == 0 || dLen == 0 || sLen != dLen {
		d.LogDW.LogServer(utils.ErrorLog).Println("List column empty or size not map!")
		return errors.New("list column empty or size not map")
	}
	// connect db source
	db, _ := d.Conn.ConnectDB(sPath)
	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			return
		}
	}(db)
	sSQLTable := `SELECT `
	for _, col := range sCols {
		sSQLTable += col + `, `
	}
	sSQLTable = strs.TrimSuffix(sSQLTable, `, `)
	sSQLTable += ` From ` + sTable
	d.LogDW.LogServer(utils.DebugLog).Println("Sql get data source table: ", sSQLTable)
	sData, err := db.Query(sSQLTable)
	defer func(sData *sql.Rows) {
		err = sData.Close()
		if err != nil {
			return
		}
	}(sData)
	if err != nil {
		d.LogDW.LogServer(utils.ErrorLog).Printf("Get data table %s fail: %v \n", sTable, err)
		return err
	}
	// Save db to destination table
	infoFields, err := sData.Columns()
	if err != nil {
		d.LogDW.LogServer(utils.ErrorLog).Printf("Not get infoField failed: %s\n", err.Error())
		return err
	}
	infoFs := make([]sql.RawBytes, len(infoFields))
	scanData := make([]interface{}, len(infoFs))
	for i := range infoFs {
		scanData[i] = &infoFs[i]
	}

	// create sql insert
	sqlInsert := `INSERT INTO ` + dTable + ` ( `
	for _, c := range dCols {
		sqlInsert += c + `, `
	}
	sqlInsert = strs.TrimSuffix(sqlInsert, `, `)
	sqlInsert += `) VALUES `
	for sData.Next() {
		sqlInsert += `(`
		err = sData.Scan(scanData...)
		if err != nil {
			d.LogDW.LogServer(utils.ErrorLog).Printf(" Scan data fail: %v\n", err)
			return err
		}
		for _, v := range infoFs {
			if string(v) == "" {
				sqlInsert += `NULL, `
			} else {
				sqlInsert += `'` + string(v) + `'` + `,`
			}
		}
		sqlInsert = strs.TrimSuffix(sqlInsert, `,`)
		sqlInsert += `),`
	}
	sqlInsert = strs.TrimSuffix(sqlInsert, `,`)
	d.LogDW.LogServer(utils.DebugLog).Println(" Sql insert: ", sqlInsert)
	// Create a new context to begin a transaction

	dbDestination, _ := d.Conn.ConnectDB(dPath)
	defer func(db *sql.DB) {
		err = db.Close()
		if err != nil {
			return
		}
	}(db)
	ctx := context.Background()
	tx, err := dbDestination.BeginTx(ctx, nil)
	if err != nil {
		d.LogDW.LogServer(utils.ErrorLog).Printf("Begin transaction failed: %v \n", err)
		return err
	}
	// save to db
	_, err = tx.ExecContext(ctx, sqlInsert)
	if err != nil {
		d.LogDW.LogServer(utils.ErrorLog).Printf("Insert data failed: %v\n", err)
		errRB := tx.Rollback()
		if  errRB != nil {
			d.LogDW.LogServer(utils.ErrorLog).Printf("Rollback data failed: %v\n", errRB)
			return errRB
		}
		return err
	}

	// commit transaction
	err = tx.Commit()
	if err != nil {
		return err
	}
	fmt.Printf("Insert data from table %s to table %s success \n", sTable, dTable)
	return nil
}

//writeToCSV
func writeToCSV(file string, columns []string, totalValues [][]string, lg *logdwhtf.Logger) error  {
	lg.LogServer(utils.InfoLog).Println(" Start export file: ",file)
	//create file
	f, err := os.Create(file)
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {

		}
	}(f)
	if err != nil {
		lg.LogServer(utils.ErrorLog).Printf(" Create file %s err %v \n", file, err )
		return err
	}
	w := csv.NewWriter(f)
	for i, row := range totalValues {
		//First write column name + first row of data
		if i == 0 {
			errCol := w.Write(columns)
			if errCol != nil {
				return errCol
			}
			errRow := w.Write(row)
			if errRow != nil {
				return errRow
			}
		} else {
			errRow := w.Write(row)
			if errRow != nil {
				return errRow
			}
		}
	}
	w.Flush()
	lg.LogServer(utils.InfoLog).Println(" Finished processing: ", file)
	return nil
}

func convertData(data []string) []string {
	out := make([]string, len(data))
	out[0] = data[0]
	var decimalDigit, size, prKey string
	typeC := data[1]
	if strs.Index(data[1],"(") != -1 {
		typeC = data[1][: strs.Index(data[1],"(")] + data[1][(strs.Index(data[1],")") + 1):]
		size = data[1][strs.Index(data[1],"(") + 1 : strs.Index(data[1],")")  ]
	}
	if strs.Index(size, ",") != -1 {
		decimalDigit = size
		size = ""
	}
	out[1] = typeC
	out[2] = data[2]
	out[3] = decimalDigit
	out[4] = size
	if data[3] == "PRI" {
		prKey = "YES"
	}
	out[5] = prKey
	return out
}

func readFile(fileName string, lg *logdwhtf.Logger) ([][]string, error )  {
	csvFile, err := os.Open(fileName)
	if err != nil {
		lg.LogServer(utils.ErrorLog).Printf(" Can't open file %s err %v:", fileName, err)
		return nil, err
	}
	defer func(csvFile *os.File) {
		err = csvFile.Close()
		if err != nil {
			return
		}
	}(csvFile)
	csvLines, errFile := csv.NewReader(csvFile).ReadAll()
	if errFile != nil {
		lg.LogServer(utils.ErrorLog).Printf("Can't read file %s because %v:", fileName, err)
		return nil, err
	}
	return  csvLines, nil
}
