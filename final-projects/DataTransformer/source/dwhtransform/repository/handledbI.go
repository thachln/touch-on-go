package repository

type HandleDBI interface {
	ExportStruct(pathFile string, outFolder string) error
	ImportStruct(pathFile string, inputFolder string) error
	TransferData(sPath string, dPath string, fMap string) error
}
