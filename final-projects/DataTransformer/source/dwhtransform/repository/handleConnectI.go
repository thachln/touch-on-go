package repository

import (
	"database/sql"
	"gitlab.com/ducpt51/dwhtf/model"
)

type HandleConnectI interface {
	ConnectDB(fileName string) (db *sql.DB, conf *model.ConfigDB)
}
