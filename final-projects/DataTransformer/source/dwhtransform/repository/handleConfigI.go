package repository

import "gitlab.com/ducpt51/dwhtf/model"

type ProcessConfigI interface {
	GetConfig(fileName string) (model.ConfigDB, error)
}
