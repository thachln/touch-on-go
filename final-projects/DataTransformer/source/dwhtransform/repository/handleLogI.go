package repository

import (
	"log"
)

type LoggerI interface {
	LogServer(typeLog string) *log.Logger
}
