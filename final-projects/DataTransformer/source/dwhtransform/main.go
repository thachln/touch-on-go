package main

import (
	"fmt"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/utils"
	"log"
	"os"
	"os/exec"
)

const (
	serverKey = "dwhtf"
	exTableKey = "genschema"
	impTableKey = "importschema"
	transferDataKey = "transfer"
)
func main() {
	logDWHTF := new(logdwhtf.Logger)
	logDWHTF.LogServer("InfoLog").Println("Starting the application...")
	// validate
	if len(os.Args) < 3 {
		logDWHTF.LogServer("ErrorLog").Println("Missing parameter:")
		return
	}
	var action string
	var pathFile string
	var folder string
	var mapFile string
	//conn := database.NewConnectDB(logDWHTF)
	//hdlDB := handler.NewProcessDB(logDWHTF, conn)
	if len(os.Args) < 4 {
		logDWHTF.LogServer("ErrorLog").Println("Missing parameter:")
		return
	}
	action = os.Args[1]
	pathFile = os.Args[2]
	if action == transferDataKey {
		mapFile = os.Args[3]
	}
	folder = os.Args[3]
	switch action {
	case exTableKey:
		pathFile += "/source.yml"
		cmd := exec.Command("genschema", "source.yml", folder)
		err := cmd.Run()
		if err != nil {
			logDWHTF.LogServer(utils.ErrorLog).Println("Call program genschema fail")
			log.Fatal(err)
		}
	case impTableKey:
		pathFile += "/dest.yml"
		cmd := exec.Command("importschema", "dest.yml", folder)
		err := cmd.Run()
		if err != nil {
			logDWHTF.LogServer(utils.ErrorLog).Println("Call program importschema fail")
			log.Fatal(err)
		}
	case transferDataKey:
		cmd := exec.Command("transfer", "source.yml", "dest.yml", mapFile)
		err := cmd.Run()
		if err != nil {
			logDWHTF.LogServer(utils.ErrorLog).Println("Call program transfer fail")
			log.Fatal(err)
		}
	default:
		fmt.Println("Please enter sub_cmd correct!")
	}
	logDWHTF.LogServer("InfoLog").Println("End the application...")
}
