package main

import (
	"gitlab.com/ducpt51/dwhtf/database"
	"gitlab.com/ducpt51/dwhtf/handler"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/utils"
	"log"
	"os"
)

func main() {
	logDWHTF := new(logdwhtf.Logger)
	logDWHTF.LogServer("InfoLog").Println("Starting the application...")
	if len(os.Args) < 3 {
		logDWHTF.LogServer("ErrorLog").Println("Missing parameter:")
		return
	}
	conn := database.NewConnectDB(logDWHTF)
	hdlDB := handler.NewProcessDB(logDWHTF, conn)
	logDWHTF.LogServer(utils.InfoLog).Println("Start program transfer!")
	println("Start program transfer!")
	sPath := "./config/" + os.Args[1]
	dPath := "./config/" + os.Args[2]
	err := hdlDB.TransferData(sPath, dPath, os.Args[3])
	if err != nil {
		log.Fatal(err)
	}
	logDWHTF.LogServer(utils.InfoLog).Println("End program transfer!")
	println("End program transfer!")
}
