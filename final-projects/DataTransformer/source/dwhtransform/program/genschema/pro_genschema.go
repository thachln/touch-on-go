package main

import (
	"gitlab.com/ducpt51/dwhtf/database"
	"gitlab.com/ducpt51/dwhtf/handler"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/utils"
	"log"
	"os"
)

func main() {
	logDWHTF := new(logdwhtf.Logger)
	logDWHTF.LogServer("InfoLog").Println("Starting the application...")
	if len(os.Args) < 3 {
		logDWHTF.LogServer("ErrorLog").Println("Missing parameter:")
		return
	}
	conn := database.NewConnectDB(logDWHTF)
	hdlDB := handler.NewProcessDB(logDWHTF, conn)
	logDWHTF.LogServer(utils.InfoLog).Println("Start program genschema!")
	println("Start program genschema!")
	pathFile := "./config/" + os.Args[1]
	err := hdlDB.ExportStruct(pathFile, os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	println("End program genschema!")
	logDWHTF.LogServer(utils.InfoLog).Println("End program genschema!")
	logDWHTF.LogServer("InfoLog").Println("End the application...")
}