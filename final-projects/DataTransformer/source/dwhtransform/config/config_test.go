package config

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/model"
	"gitlab.com/ducpt51/dwhtf/repository"
	"testing"
)

func initNewConfigRepository() repository.ProcessConfigI {
	var logDW *logdwhtf.Logger
	return  NewConfigRepository(logDW)
}

func TestConfRepository_GetConfig(t *testing.T) {
	i := initNewConfigRepository()
	confEX := model.ConfigDB{}
	confEX.Database .Port = "3306"
	confEX.Database.Type = "mysql"
	confAC, err := i.GetConfig("source.yml")
	assert.Nil(t, err)
	assert.Equal(t, confEX.Database.Port, confAC.Database.Port)

	//abNormal
	_, err = i.GetConfig("onfig/source.yml")
	assert.NotNil(t, err)
}