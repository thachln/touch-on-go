package config

import (
	"gitlab.com/ducpt51/dwhtf/logdwhtf"
	"gitlab.com/ducpt51/dwhtf/model"
	"gitlab.com/ducpt51/dwhtf/repository"
	"gitlab.com/ducpt51/dwhtf/utils"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type ConfRepository struct {
	LogDW *logdwhtf.Logger
}

func NewConfigRepository(logDW *logdwhtf.Logger) repository.ProcessConfigI {
	return &ConfRepository{logDW}
}

func (c ConfRepository) GetConfig(fileName string) (model.ConfigDB, error) {
	conf := new(model.ConfigDB)
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		c.LogDW.LogServer(utils.ErrorLog).Printf("yamlFile.Get err   #%v ", err)
		return model.ConfigDB{}, err
	}
	err = yaml.Unmarshal(yamlFile, conf)
	if err != nil {
		c.LogDW.LogServer(utils.ErrorLog).Fatalf("Unmarshal: %v", err)
		return model.ConfigDB{}, err
	}
	return *conf, nil

}
