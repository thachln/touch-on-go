# Project Name

dwhtransform

## Executing program
build file dwhtf.exe
```
go build -o dwhtf.exe
```
Install submodule genschema, importschema, transfer
```
go install ./program/genschema ./program/importschema ./program/transfer
```

# example
Export database:
```
dwhtf genschema config ouput_file
or
genschema source.yml  output_file
```

Import data:
```
dwhtf importschema config input_file
or
importschema dest.yml input_file
```

Transfer data:
```
dwhtf transfer config mapping.csv
or
transfer source.yml dest.yml mapping.csv
```
Unit Test:
```
go test -v -cover ./...
```
