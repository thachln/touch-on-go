package logdwhtf

import (
	"flag"
	"log"
	"os"
)

type Logger struct {
	Log *log.Logger
}

func (l *Logger) LogServer(typeLog string) *log.Logger {
	var logpath = "info.log"
	flag.Parse()
	//create file if not exists
	if _, err := os.Stat(logpath); os.IsNotExist(err) {
		file, errCreate := os.Create(logpath)
		if errCreate != nil {
			log.Fatal(errCreate)
		}
		return log.New(file, typeLog, log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		file, openErr := os.OpenFile(logpath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
		if openErr != nil {
			log.Fatal(openErr)
		}
		return log.New(file, typeLog, log.Ldate|log.Ltime|log.Lshortfile)
	}
}